#Multi-Camera System Calibration Toolbox for Matlab#

This is a toolbox for calibrating both intrinsics and extrinsics of multi-camera systems. It uses a feature-descriptor based calibration pattern. 

##Content##
* System requirements
* Quick start
* API descriptions
* Calibration examples
* References 

##System requirements##
This toolbox works on Windows, Unix and Linux system. It is recommended to use this toolbox on Matlab 2012b or higher version. It depends on Matlab functionality including SURF feature detection and matching, non-linear least square optimization. 

##API descriptor##
The toolbox currently does not provide GUI, but calibration can be easily done by using the APIs provided. All functionalities are well encapsulated in Matlab classes for customized usage. 

###Camera classes###
Two camera classes are provided to encapsulate camera operation. One class encapsulates the standard pinhole camera model; The other class encapsulates the catadioptric camera model. 
####`PinholeCamera`####
Standard perspective camera model. 
#####`obj = PinholeCamera(width, height, gamma1, gamma2, s, u0, v0, k1, k2, p1, p2)`#####
This is the constructor. `k1` and `k2` are radial distortion; `p1` and `p2` are tangent distortion. 

#####`[x, jacobPose, jacobCamera] = projectPoints(obj, points, rvec, tvec)`#####
Project a set of 3D point onto image. `rvec` is the 3D angle-axis rotation denotion. `x` is the projected image points. `jacobPose` is a 2Nx6 Jacobian matrix over 6 pose parameters (`rvec` and `tvec`). `jacobCamera` is a 2Nx9 Jacobian matrix over 9 camera intrinsic parameters. 

####`CataCamera`####
Omnidirectional catadioptric camera model. See Mei's paper for details. 

#####`obj = CataCamera(width, height, gamma1, gamma2, s, u0, v0, k1, k2, p1, p2)`#####
This is similar to `PinholeCamera` class. `xi` is the lens shape coefficient. 

#####`[x, jacobPose, jacobCamera] = projectPoints(obj, points, rvec, tvec)`#####
This is similar to `PinholeCamera` class. `jacobCamera` is a 2Nx10 Jacobian matrix. 

###Calibration classes###
####`CameraCalibrationBase`####
Base calibration class. 

#####`obj = CameraCalibrationBase(width, height, pattern)`#####
This is the constructor. `pattern` is the calibration pattern image used. 


####`PinholeCalibration`####
