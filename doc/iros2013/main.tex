\documentclass[letterpaper, 10 pt, conference]{ieeeconf}

\IEEEoverridecommandlockouts

\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{epsfig}

\title{\LARGE \bf
A Multiple Camera System Calibration Toolbox Using Feature Descriptor Based Calibration Pattern
}

\author{
Bo Li, Lionel Heng, Kevin K\"oser and Marc Pollefeys
}

\begin{document}

\maketitle

\begin{abstract}
This paper presents a newly designed, feature descriptor based calibration pattern and a toolbox using it to easily calibrate both intrinsics and extrinsics of a multiple camera system. 

Compared with existing calibration patterns e.g. chessboard, the proposed pattern contains much more sufficient features of different scales and these features can be easily automatically detected. 

The proposed toolbox supports calibration of camera system made up of either normal pinhole cameras or catadioptric cameras. The calibration only requires that neighbor cameras can partly see calibration pattern at the same time and the seen parts can even be different. No overlapping field of view is required. We show that this toolbox can be easily used to automatically calibrate normal camera systems. 

\end{abstract}



\section{Introduction}
Multiple camera systems have become increasingly prevalent in robotics and computer vision research. These systems include mature products e.g. stereo camera or Ladybug camera, and also a large variety of customized camera systems. Commonly used cameras in the systems include normal pinhole cameras, fish-eye cameras and catadioptric cameras. To make such systems to work properly, both the intrinsics and extrinsics of the cameras should be well calibrated. 

In the past years, many efficient methods have been developed for calibrating the intrinsics of different kinds of cameras. These methods can be categorized as calibration with special calibration object and self-calibration. In this paper, we mainly discuss the first category which is usually much more accurate than self-calibration. Many toolboxes are available for this category of methods. For calibrating a pinhole camera, \cite{zhang2000flexible} has been one of the most representative method. Some popular calibration toolboxes, e.g. \cite{bouguet2004camera}, \cite{stoyanov2006camera} are inspired by this method. For generic cameras, \cite{scaramuzza2009omnidirectional} proposed a toolbox to use polynomial to approximate rays corresponding to each image point. This method generically applies to most camera models but does not provide closed form solution to undistort raw images. In the toolbox proposed in \cite{mei10omnidirectional}, a unified catadioptric model is proposed for calibrating catadioptric system, fisheye camera and sphere mirror system. This model can be interpreted similar with \cite{scaramuzza2009omnidirectional} but parametrizes rays instead of using an arbitrary polynomial, which makes undistortion much simpler. 

\begin{figure}
\centering
\includegraphics[width=0.35\textwidth]{images/patternsample.eps}
\caption{Top: An example for the proposed calibration pattern. Middle and bottom row: Noise image components at different frequency. }
\label{PatternFig}
\end{figure}

Some toolboxes are also available to calibrate simple multiple camera systems. For example, in \cite{bouguet2004camera} users can find functionality to calibrate stereo system made up of two neighboring cameras. For calibration on system of more than two cameras, toolboxes \cite{svoboda2005convenient}, \cite{easycal} can be used. These calibration toolboxes all make use of the overlapping field of view of cameras. These toolboxes can calibrate systems like stereo camera or some camera rig with cameras on the rig all pointing inside the rig. These toolboxes are not suitable to calibrate system of cameras with no or little overlapping field of view. For example, it is recently prevalent to mount on vehicle platform multiple cameras pointing to different direction. This system can not be calibrated using existing calibration toolboxes due to the narrow field of view. Some hand-eye calibration algorithms \cite{} can be used to calibrate this system but requires reconstructing visual odometry for each camera and the result can be not stable due to the visual odometry drift. 

Besides camera models, many researches have also been drawn into development of accurate and easy-to-use calibration pattern. Early researches make use of cubes with chessboard or circle dots on its surface \cite{bouguet2004camera}. This is not very convenient since a perfect cube is not easy to built. The state-of-art calibration systems mainly make use of calibration board, which is a plane with chessboard or circle dots printed. Automatic detector for these patterns is also available. Some comparison between chessboard and circle dots can also be found \cite{mallon2007pattern}. Similar but even simplified calibration object like single light dot can also be found in \cite{easycal}, \cite{easycal} for calibrating multiple camera system. One disadvantage of these pattern is that cameras have to see the entire pattern in the calibration images, which makes the pattern not applicable to calibrate extrinsics of cameras with little overlapping field of view. In addition to chessboard or circle dots, some other patterns have also been proposed. \cite{schmalz2011camera} used temporal coded pattern to calibrate  cameras. This method used Gray code to match world point and image point so it does not require the entire pattern to be inside image. The poor flexibility is a drawback of this method, e.g. a display based calibration is very inconvenient in calibrating cameras mounted on a car. 

In this paper, we propose a new calibration pattern which codes each image point using feature extraction techniques. Our pattern contains a large number of detectable features. The pattern can be recognized and located even if it is only partly seen in the photo. Thus to calibrate extrinsics between cameras without overlapping field of view, we only need to use a long enough pattern so that each of the two cameras can seen one end of the pattern. Based on this idea, our toolbox calibrate the relative extrinsics between each neighbor camera pair and then execute a bundle adjustment to unify camera poses in a same coordinate system. The calibration on a camera system is as convenient as calibrating a single camera using traditional chessboard. 



\section{Calibration Pattern}
\label{PatternSec}
\subsection{Feature Detection Revisited}
Point-feature detection has been a successful computer vision technique used in researches like sparse reconstruction, object detection and etc. A point feature usually contains two components: key point and descriptor. Take the most widely used feature SIFT as a example. A Difference of Gaussian filter (DoG) is used to detect key points. This detection is executed on both the original image and shrunken images, which guarantees key point on different scales can be detected. The image gradient with neighborhood of a key point is counted in a histogram as the descriptor for the features. Details about the SIFT feature can be found in \cite{lowe2004distinctive}. A variation of SIFT, SURF feature is also a widely used feature technique \cite{bay2006surf}. It replaces SIFT in many applications due to the its computational efficiency. 

\subsection{Reversed Engineering}
The basic idea of the proposed calibration pattern is to design a pattern sufficient of detectable  features. Meanwhile descriptor of the features should distinguishable so that detected points in a image can be uniquely matched to its correspondence in the calibration pattern. 

To trigger feature detection, we used several noise image to compose a calibration pattern according to the mechanism of SIFT/SURF feature. A noise image can invoke sufficient response from DoG filter. A problem of high frequency noise image the blur effect. For a grayscale image, if camera is located far 
away, then the noise image is percepted as pure gray. The solution for this problem is to compose images with noise from multiple scale. In our implementation, we generated noise image of different size and resize them to the same size. These images with noise of different scales are then added up together. This procedure can be interpreted as a reverse engineering of the scaling procedure in SIFT/SURF detection. Thus the result image has sufficient features on different scales. This the pattern detectable for camera location at different distance. The following Matlab code provides a example to generate a $600 \times 800$ calibration pattern. Fig.\ref{PatternFig} shows a calibration pattern at its top. Its components on different scales is shown at its bottom. 
\begin{center}

\mbox{
\parbox{0.36\textwidth}{
\footnotesize
\texttt{N = 600; M = 800; \\
pattern = 0; count = 0; \\
m = 4; \\
while m < M\\ 
\hspace*{14pt}    n = round(N / M * m); \\
\hspace*{14pt}    noise = rand(n, m); \\
\hspace*{14pt}    noise = imresize(noise, [N, M]); \\
\hspace*{14pt}    pattern = pattern + noise; \\
\hspace*{14pt}    count = count + 1; \\
\hspace*{14pt}    m = m * 2; \\
end\\
pattern = pattern ./ count; \\
pattern = histeq(pattern); \\
imshow(pattern); }
}}
\end{center}

\subsection{Feature Matching}
Feature detection and matching between two images have become two standard steps of modern 3D vision pipeline. In our calibration approach, similar procedure is executed between each photo and the original known pattern. Similarly to the feature matching procedure in 3D vision, we match the pattern points and photo points in the following steps. 

First, features detected from photo and pattern are matched according to the descriptor similarity. We used well-known similarity ratio check proposed in \cite{lowe2004distinctive}. The best match of a query feature is accepted only if their descriptor distance is smaller than 0.6 times the distance between the query feature and its second best match. 

Second, a \textit{fundamental matrix for radial distortion} is estimated by RANSAC between photo points and pattern points to filter inliers. Note that this fundamental matrix is not the same used in traditional epipolar geometry. The traditional one requires that both point sets need to be from rectified image. This fundamental matrix is proposed by research from \cite{}. We provide some simple explanation about the fundamental matrix here. Denote $p^d$ as a distorted photo point, $p^u$ as its corresponding undistorted point and $p^c$ as the corresponding point on calibration pattern. If the photo only has radial distortion and $e$ is the distortion center. We can write:
\begin{equation}
p^d = e + \lambda (p^u - e)
\end{equation}
where $\lambda$ is a distortion coefficient corresponding to $p^u$. In addition since $x_u$ can be obtained by a perspective transform from $p^c$, there exist a homography $H$ s.t. $p^u = H p^c$. Substitute this into the above equation and multiply the equation by $[e]_\times$, we obtain: 

\begin{eqnarray}
p^d =& e + \lambda (H p^c - e)\\
{}[e]_\times p^d =& \lambda [e]_\times H p^c \\
0 =& {p^d}^\top [e]_\times H p^c
\end{eqnarray}
$F \equiv [e]_\times H$ is the fundamental matrix for radial distortion and $e$ can be interpreted as image principle points. This model suits well for both pinhole camera model and generic omnidirectional camera. In the proposed calibration approach, this model is used as a strong constraint to remove fake feature matches. 

Third, to double check remaining wrong matches. A homography is estimated using RANSAC over the remaining matches. Since homography can not well model lens distortion, the estimation uses a very loose threshold e.g. 100px. 

\section{Single Camera Calibration}
Single camera calibration estimate intrinsics of a camera and poses of calibration pattern w.r.t. the camera. This estimate provides initial information to multiple camera extrinsics calibration. 
\subsection{Camera Models}
Consider a point $(u, v, \gamma)^\top$ on an image plane with focal length equals to $\gamma$ and principle point $(0, 0)^\top$. It corresponds to ray which goes into camera with its direction $(u, v, f(u, v))^\top$. This denotion can unify pinhole camera model and omnidirectional camera model by defining different $f(u, v)$. Here are some examples, for a pinhole camera:
\begin{equation}
f(u, v) \equiv \gamma
\label{pinholeEqn}
\end{equation}
For a catadioptric system with its lens distortion parameter $\xi = 1$:
\begin{equation}
f(u, v) = \frac{\gamma}{2} - \frac{1}{2 \gamma} \rho^2
\label{parabolaEqn}
\end{equation}
where $\rho = \sqrt{u^2 + v^2}$. Details about (\ref{parabolaEqn}) can be found in \cite{mei10omnidirectional}. To denote more generalized camera model, $f(u, v)$ can be parametrized as a general polynoimal of $\rho$, as proposed in \cite{scaramuzza2009omnidirectional}: 
\begin{equation}
f(u, v) = a_0 + \cdots + a_n \rho^n
\label{polynEqn}
\end{equation}
(\ref{polynEqn}) generally does not have a closed form inverse transform. In the proposed toolbox, we used the catadioptric model proposed by \cite{mei10omnidirectional} to model omnidirectional camera. This model has one parameter $xi$ to model the omnidirectional camera effect and remains the same with pinhole camera with 	other intrinsic parameters. The other intrinsics includes: focal length $\gamma_1$ and $\gamma_2$, aspect ratio $s$, principle point $(u_0, v_0)$, radial and tangent lens distortion parameter $k1, k2, p1, p2$. 

\subsection{Initialization}
In \cite{mei10omnidirectional}, its model assumes the catadioptric coefficient $xi = 1$ for initialization and then refine the estimation for all intrinsics and extrinsics. The initialization is very simple and can generate good initial guess for all parameter. The limitation of the initialization is that it requires a known projection of a straight line on the pattern. This is easy to obtain from chessboard but different for the proposed pattern. Fortunately, the initialization can be solved by substituting Mei's model into Scaramuzza's initialization. Readers can also refer to \cite{scaramuzza2009omnidirectional} for the initialization part. 

In initialization we assume two focal lengths to be the same, principle point to be the center the photo and ignore lens distortion. For a detected feature point we compute corresponding $u$ and $v$ by subtracting its coordinates with image center. 

For a homogeneous coordinate of a pattern point $p^c = (x, y, 1)^\top$, denote its corresponding 3D point as $(x, y, 0, 1)^\top$. We have the projection equation for this 3D point and its corresponding camera ray $(u, v, f(u, v))^\top$: 
\begin{equation}
\begin{split}
\mu \left(
	\begin{array}{c}
	u \\ v \\ f(u, v)
	\end{array}
\right)
=& \left( 
	\begin{array}{cccc}
	r_1 & r_2 & r_3 & t
	\end{array}
\right) 
\left(
	\begin{array}{c}
	x \\ y \\ 0 \\ 1
	\end{array}
\right)\\
=& \left( 
	\begin{array}{ccc}
	r_1 & r_2 & t
	\end{array}
\right) p^c
\end{split}
\end{equation}
Multiply the equation with $[(u, v, f(u, v))^\top]_\times$, we have: 
\begin{equation}
0 = [(u, v, f(u, v))^\top]_\times 
\left( 
	\begin{array}{ccc}
	r_1 & r_2 & t
	\end{array}
\right) p^c
\label{initEqn}
\end{equation}
Denote the rotation axes $r_i = (r_{1i}, r_{2i}, r_{3i})^\top$ and the translation $t = (t_1, t_2, t_3)^\top$. The third row of this equation is independent from $f(u, v)$ and is a linear equation w.r.t to unknowns $r_{11}, r_{21}, r_{12}, r_{22}, t_1, t_2$:
\begin{equation}
u (r_{21} x + r_{22} y + t_2) - v (r_{11} x + r_{12} y + t_1) = 0
\end{equation}
For each photo, we can substitute its point correspondences in this equation and stack them as a linear system. Solving this system we obtain $r_{11}, r_{21}, r_{12}, r_{22}, t_1, t_2$ up to a unknown scale. $r_{31}, r_{32}$ and the scale can be determined by exploiting the unit and perpendicularity constraint of $r_1$ and $r_2$. Note that multiple solutions exists at this stage, wrong estimation will be rejected after estimating remaining unknowns $\gamma$ and $t_3$. 

Solving $\gamma$ and $t_3$ requires using the first and second row in (\ref{initEqn}):
\begin{eqnarray}
v (r_{31} x + r_{32} y + t_3) - f(u, v) (r_{21} x + r_{22} y + t_2) = 0 \\ 
f(u, v) (r_{11} x + r_{12} y + t_1) - u (r_{31} x + r_{32} y + t_3) = 0
\end{eqnarray}

For pinhole camera model, $f(u, v)$ is replaced by $\gamma$. Substituting $r_{11}, r_{21}, r_{12}, r_{22}, t_1, t_2$, we obtained linear equations about $\gamma$ and $t_3$. The two unknowns are then solved by forming linear system of multiple detected correspondences. 

For omnidirectional camera, (\ref{parabolaEqn}) is substituted into $f(u, v)$. We regard $\gamma$, $\frac{1}{\gamma}$ and $t_3$ as 3 unknowns and solve them by forming linear system similar to the above. Note that we ignored a constraint by treating $\gamma$ and $\frac{1}{\gamma}$ as two unknowns. 

The initialization will return multiple solutions on intrinsics and extrinsics. The correct one can be selected by checking reprojection error. 

The toolbox initializes $\gamma$ and extrinsics for each input photo and selects the median of $\gamma$ as initial estimate. 

\subsection{Refinement}
Based on the initial estimate, the toolbox then refine all intrinsics and extrinsics parameters using the Levenberg-Marquadt algorithm. Further details about the optimization can be found in \cite{mei10omnidirectional} and \cite{scaramuzza2009omnidirectional}. 

\begin{figure}
\centering
\includegraphics[width=0.3\textwidth]{images/graphsample.eps}
\caption{•}
\end{figure}


\section{Extrinsics Calibration}
In this part, we assume that cameras are all fixed, and during calibration we move calibration pattern around cameras. Calibration on single cameras provides an estimate for the poses of pattern. If cameras are synchronized to take images of the pattern at the same time, the relative poses of the pattern w.r.t each camera are known. Thus camera poses can be extracted from these relative poses of the pattern. The toolbox initializes camera poses in this way and optimizes them using a bundle-adjustment-like method. 
\subsection{Initialization}
We create a pose graph to denote the calibration scenario. Each camera is denoted by a camera vertex $camera_i$ in the graph. Meanwhile, each pose of the pattern is also denoted by a pattern vertex $pattern_i$. If $camera_i$ takes a photo of the pattern at pose $pattern_j$, then $camera_i$ and $pattern_j$ are connected by a photo edge, denoted by $photo_{i, j}$. Each edge uniquely corresponds to one photo. Note that this graph is a bipartite graph and an edge always have one camera vertex and one pattern vertex. 

Vertices in the pose graph can be used to store the poses of cameras and pattern in the world coordinate system. Edges can be used to store the relative pose transform between its two end vertices. For a photo of the pattern at pose $pattern_j$ taken by $camera_i$, we have the relative pose of $pattern_j$ w.r.t. $camera_i$ from single camera calibration.  

Assume the world coordinate system is aligned with $camera_1$, pose of all vertices connected to $camera_1$ can be obtained by following the relative transforms stored on the path from $camera_1$. In practice, if two cameras see the pattern at the same time in their photos, then the two cameras are connected via two photo edges and one pattern vertex. 

In the proposed toolbox implementation, the above pose graph is first built based on the result on single calibration calibration. Next a spanning tree with $camera_1$ as its root is extracted using breath first search. Vertex poses is then computed by traversing the spanning tree from $camera_1$ and following the relative transform along edges. Thus we have a initial pose estimation for all vertices in world coordinate system. 

\subsection{•}

\section{Experiment}
\begin{figure}
\centering
%\begin{tabular}{cc}
\includegraphics[width=0.2\textwidth]{images/left000.eps} \includegraphics[width=0.2\textwidth]{images/right000.eps} \\ 
\vspace{3pt}
\includegraphics[width=0.2\textwidth]{images/left001.eps} 
\includegraphics[width=0.2\textwidth]{images/right001.eps} 
%\end{tabular}

\caption{}
\end{figure}


\begin{figure}
\centering
\includegraphics[trim=0in 0.3in 0in 0in, clip=true, scale=0.56]{images/stereo3dthin.eps}
\caption{}
\end{figure}



\section{Acknowledgement}

\bibliographystyle{IEEEtran}
\bibliography{main}


\end{document}