classdef CameraCalibInfo < handle
    properties
        
        % Settings
        minMatchedPoints = 20; 
        
        % Parameters
        camera
        
        % Data
        pattern
        cameraParam
        photosInfo
    end
    
    methods
        % 
        function cameraCalibInfo = CameraCalibInfo(width, height, pattern)
            cameraCalibInfo.pattern = pattern; 
            cameraCalibInfo.photosInfo = []; 
            cameraCalibInfo.camera = CataCamera(width, height); 
        end
        
        % 
        function cameraCalibInfo = addPhoto(cameraCalibInfo, photo)
            if (size(photo, 3) > 1) 
                photo = rgb2gray(photo); 
            end
            
            cameraCalibInfo.photosInfo(end + 1).photo = photo; 
            
            patternKeyPoints = detectSURFFeatures(cameraCalibInfo.pattern, 'NumOctaves', 8); 
            photoKeyPoints = detectSURFFeatures(photo, 'NumOctaves', 8); 

            [patternFeatures, patternPoints] = extractFeatures(cameraCalibInfo.pattern, patternKeyPoints);
            [photoFeatures, photoPoints] = extractFeatures(photo, photoKeyPoints);
            
            indexPairs = matchFeatures(patternFeatures, photoFeatures, 'Method', 'NearestNeighborRatio');
            
            if (size(indexPairs, 1) < cameraCalibInfo.minMatchedPoints)
                cameraCalibInfo.photosInfo(end).valid = false; 
                return; 
            end
            
            patternPoints = patternPoints(indexPairs(:, 1));
            photoPoints = photoPoints(indexPairs(:, 2));
            
            patternPoints = patternPoints(:).Location;
            photoPoints = photoPoints(:).Location;
            
            [~, inliersMask] = estimateFundamentalMatrix(patternPoints, photoPoints, 'Method', 'RANSAC');
            
            if (sum(inliersMask) < cameraCalibInfo.minMatchedPoints)
                cameraCalibInfo.photosInfo(end).valid = false; 
                return; 
            end
            
            patternPoints = patternPoints(inliersMask, :);
            photoPoints = photoPoints(inliersMask, :);
            
            
            cameraCalibInfo.photosInfo(end).valid = true; 
            cameraCalibInfo.photosInfo(end).patternPoints = patternPoints; 
            cameraCalibInfo.photosInfo(end).photoPoints = photoPoints; 
            cameraCalibInfo.photosInfo(end).nPoints = size(patternPoints, 1); 
        end
        
        % 
        function cameraCalibInfo = calibrate(cameraCalibInfo)
            
        end
        
        % 
        function cameraCalibInfo = initializeCalibration(cameraCalibInfo)
            u0 = cameraCalibInfo.camera.width / 2; 
            v0 = cameraCalibInfo.camera.height / 2; 
            
            photosValid = find(cameraCalibInfo.photosInfo(:).valid)'; 
            
            for i = photosValid
                x(:, i) = cameraCalibInfo.photosInfo(selected).patternPoints(:, 1); 
                y(:, i) = cameraCalibInfo.photosInfo(selected).patternPoints(:, 2); 
                u(:, i) = cameraCalibInfo.photosInfo(selected).photoPoints(:, 1) - u0; 
                v(:, i) = cameraCalibInfo.photosInfo(selected).photoPoints(:, 2) - v0; 
            end
            % Form equation for the Essential Matrix 
            M = [-v(:).*x(:), -v(:).*y(:), u(:).*x(:), u(:).*y(:), -v(:), u(:)]; 
            [~, ~, V] = svd(M); 
            r11 = v(1, end); 
            r21 = v(3, end); 
            r12 = v(2, end); 
            r22 = v(4, end); 
            t1 = v(5, end); 
            t2 = v(6, end); 
            
            r31s = sqrt(roots([1, r11^2 + r21^2 - r12^2 - r22^2, -(r11*r12 + r21*r22)^2])); 
            r31s = [r31s; -r31s]; 
            for r31 = r31s'
                
                r32 = -(r11*r12 + r21*r22) / r31; 
                
                scale = 1 / norm(r1); 
                r1 = [r11; r21; r31] .* scale; 
                r2 = [r12, r22, r32] .* scale; 
                t = t .* scale; 
                
                A = []; 
                for i = photosValid
                    block = [r1(2) .* x(:, i) + r2(2) * y(:, i), ]
                end
                
            end
                        
            cameraCalibInfo.camera.gamma1 = gammaGuess; 
            cameraCalibInfo.camera.gamma2 = gammaGuess; 
            cameraCalibInfo.camera.u0 = u0; 
            cameraCalibInfo.camera.v0 = v0; 
            cameraCalibInfo.camera.xi = 1; 
            cameraCalibInfo.camera.k1 = 0; 
            cameraCalibInfo.camera.k2 = 0; 
            cameraCalibInfo.camera.p1 = 0; 
            cameraCalibInfo.camera.p2 = 0; 
            

            
           
        end
    end
end

        
        
        